# Runing multiple containers -Docker Compose



## The Project:
This is similar to the project devoloping locally with docker. The different is instead of pull the MongoDB and Mongo-Express images using the CLI, a yaml file is created containing both images. These images will be in same network, thereby communicate by default. 

```
Technology Used:
Docker, MongoDB, Mongo-Express, Docker-Compose
```

```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/runing-multiple-containers-docker-compose.git
git branch -M main
git push -uf origin main
```

